<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class StijmeerDbRestoreCommand.
 *
 * Use:
 * $ php console stijmeer:db:restore
 */
class StijmeerDbRestoreCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stijmeer:db:restore')
            ->setDescription('Restores the database from latest backup SQL dump')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        // Get variables from `app/config/parameters.yml`
        $dbName = $container->getParameter('database_name');
        $dbUsername = $container->getParameter('database_user');
        $dbPassword = $container->getParameter('database_password');
        $dbDumpPath = $container->getParameter('database_dump_path');

        $command = "MYSQL_PWD=${dbPassword} mysql --user=${dbUsername} ${dbName} < ${dbDumpPath}/latest.sql";
        exec($command);

        $output->writeln("Backup for database `${dbName}` restored!");
    }

}
