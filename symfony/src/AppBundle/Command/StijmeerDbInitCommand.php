<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class StijmeerDbInitCommand.
 *
 * Use:
 * $ console stijmeer:db:init
 */
class StijmeerDbInitCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stijmeer:db:init')
            ->setDescription('Initializes the database by creating database user and database')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        // Get variables from `app/config/parameters.yml`
        $dbName = $container->getParameter('database_name');

        $application = $this->getApplication();

        $commands = [
            'stijmeer:db:user',
            'doctrine:database:create',
            'doctrine:schema:create',
            'stijmeer:db:seed',
        ];

        foreach ($commands as $commandName) {
            $parameters = [
                'command' => $commandName,
            ];
            $commandInput = new ArrayInput($parameters);

            $application
                ->find($commandName)
                ->run($commandInput, $output);
        }

        $output->writeln("Database `${dbName}` initialized!");
    }

}
