<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class StijmeerDbSeedCommand.
 *
 * Use:
 * $ php console stijmeer:db:seed
 */
class StijmeerDbSeedCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('stijmeer:db:seed')
            ->setDescription('seeds the database from sql dump')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        // Get variables from `app/config/parameters.yml`
        $dbName = $container->getParameter('database_name');
        $dbUsername = $container->getParameter('database_user');
        $dbPassword = $container->getParameter('database_password');
        $dbDumpPath = $container->getParameter('database_dump_path');

        // original command
        $command = "MYSQL_PWD=${dbPassword} mysqldump --user=${dbUsername} ${dbName} < ${dbDumpPath}/source/offices.sql";
        // new command
        $command = "MYSQL_PWD=${dbPassword} mysql --user=${dbUsername} ${dbName} < ${dbDumpPath}/source/offices.sql";
        exec($command);

        $output->writeln("Database `${dbName}` seeded!");
    }

}
