<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class OfficesController extends Controller
{
    /**
     * @Route("/api/offices")
     */
    public function officesSearch(Request $request)
    {
        // Get parameters from querystring
        $location = $request->query->get('location');
        $range = $request->query->get('range');
        $weekend = $request->query->get('weekend');
        $support = $request->query->get('support');

        // Get location and latitude for the location
        $geocodedLocation = $this->container
            ->get('bazinga_geocoder.geocoder')
            ->using('google_maps')
            ->geocode($location);
        $address = $geocodedLocation->first();
        $locationLat = $address->getLatitude();
        $locationLng = $address->getLongitude();

        // Create address array
        $address = array(
            'city' => $location,
            'latitude' => $locationLat,
            'longitude' => $locationLng,
        );

        // Create query array
        $query = array(
            'location' => $address,
            'range' => $range,
            'weekend' => $weekend,
            'support' => $support
        );

        // Get results from database
        $offices = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Offices');

        /*
         * Only select offices within square range of location
         *
         * Equator: 1deg LAT = 110.57 km, 1deg LNG = 111.32 km
         * Poles: 1deg LAT = 111.69 km
         * */

        $rangeDegrees = $range / 111.7; // Every lat|lng degree° is ~ 111Km
        $min_lat = $locationLat - $rangeDegrees;
        $max_lat = $locationLat + $rangeDegrees;
        $min_lng = $locationLng - $rangeDegrees;
        $max_lng = $locationLng + $rangeDegrees;

        // Build query
        $dbQuery = $offices->createQueryBuilder('o')
            ->select('o.id, o.street, o.city, o.latitude, o.longitude, o.isOpenInWeekends, o.hasSupportDesk')
            ->where('o.latitude BETWEEN :min_lat AND :max_lat AND o.longitude BETWEEN :min_lng AND :max_lng')
            ->setParameter('min_lat', $min_lat)
            ->setParameter('max_lat', $max_lat)
            ->setParameter('min_lng', $min_lng)
            ->setParameter('max_lng', $max_lng)
            ->orWhere('o.city = :city')// Also include offices with same city name but out of range
            ->setParameter('city', $location);
        // Filter if open in weekends
        if ($weekend == "Y") {
            $dbQuery
                ->andWhere('o.isOpenInWeekends = :weekend')
                ->setParameter('weekend', $weekend);
        }
        // Filter if support desk
        if ($support == "Y") {
            $dbQuery
                ->andWhere('o.hasSupportDesk = :support')
                ->setParameter('support', $support);
        }
        // Execute query
        $offices = $dbQuery->getQuery()->getResult();

        // Filter from square radius to circular radius
        $officesRange = [];
        foreach ($offices as $office) {
            $distance = $this->vincentyGreatCircleDistance($office["latitude"], $office["longitude"], $locationLat, $locationLng);
            if ($distance <= $range) {
                $office["distance"] = $distance;
                array_push($officesRange, $office);
            }
        }

        // Sort offices by distance
        usort($officesRange, function ($a, $b) {
            return $a['distance'] <=> $b['distance'];
        });


        $data = array(
            'query' => $query,
            'results' => $officesRange
        );

        // return result in json format
        return $this->json($data);
    }

    /**
         * Calculates the great-circle distance between two points, with the Vincenty formula.
         * Source: http://stackoverflow.com/questions/10053358/measuring-the-distance-between-two-coordinates-in-php
         *
         * @param float $latitudeFrom Latitude of start point in [deg decimal]
         * @param float $longitudeFrom Longitude of start point in [deg decimal]
         * @param float $latitudeTo Latitude of target point in [deg decimal]
         * @param float $longitudeTo Longitude of target point in [deg decimal]
         * @param float $earthRadius Mean earth radius in [m]
         * @return float Distance between points in [m] (same as earthRadius)
         */
    function vincentyGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lngFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lngTo = deg2rad($longitudeTo);

        $lngDelta = $lngTo - $lngFrom;
        $a = pow(cos($latTo) * sin($lngDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lngDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lngDelta);

        $angle = atan2(sqrt($a), $b);
        $distanceM = $angle * $earthRadius;
        $distanceKM = $distanceM / 1000;
        return round($distanceKM, 2);
    }
}
