<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Offices
 *
 * @ORM\Table(name="offices")
 * @ORM\Entity
 */
class Offices
{
    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=false)
     */
    private $street = '';

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city = '';

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="is_open_in_weekends", type="string", nullable=false)
     */
    private $isOpenInWeekends = 'N';

    /**
     * @var string
     *
     * @ORM\Column(name="has_support_desk", type="string", nullable=false)
     */
    private $hasSupportDesk = 'N';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set street
     *
     * @param string $street
     *
     * @return Offices
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Offices
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Offices
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Offices
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set isOpenInWeekends
     *
     * @param string $isOpenInWeekends
     *
     * @return Offices
     */
    public function setIsOpenInWeekends($isOpenInWeekends)
    {
        $this->isOpenInWeekends = $isOpenInWeekends;

        return $this;
    }

    /**
     * Get isOpenInWeekends
     *
     * @return string
     */
    public function getIsOpenInWeekends()
    {
        return $this->isOpenInWeekends;
    }

    /**
     * Set hasSupportDesk
     *
     * @param string $hasSupportDesk
     *
     * @return Offices
     */
    public function setHasSupportDesk($hasSupportDesk)
    {
        $this->hasSupportDesk = $hasSupportDesk;

        return $this;
    }

    /**
     * Get hasSupportDesk
     *
     * @return string
     */
    public function getHasSupportDesk()
    {
        return $this->hasSupportDesk;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
